<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ . '/vendor/autoload.php');

$loader = new \Twig_Loader_Filesystem(__DIR__ . "/templates");
$twig = new \Twig_Environment($loader, array(
    "cache" => __DIR__ . "/templates/compiled", "debug" => true, "autoescape" => true
));

if (isset($_POST['searchById'])) {
    $id = $_POST['searchById'];
    $candidates = new \model\Application\getCandidatesByIdUseCase(
        new \model\Infrastructure\MySQL\mySQLRepository(
            new \model\Infrastructure\MySQL\mySQLConnection()
        )
    );
}

$candidateList = $candidates->execute($id);

$elements = array(
    'candidates' => $candidateList
);

echo $twig->render("list.html.twig", $elements);