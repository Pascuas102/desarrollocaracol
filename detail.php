<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ . '/vendor/autoload.php');

$loader = new \Twig_Loader_Filesystem(__DIR__ . "/templates");
$twig = new \Twig_Environment($loader, array(
    "cache" => __DIR__ . "/templates/compiled", "debug" => true, "autoescape" => true
));

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $candidate = new \model\Application\getCandidateByDataBaseId(
        new \model\Infrastructure\MySQL\mySQLRepository(
            new \model\Infrastructure\MySQL\mySQLConnection()
        )
    );

    $candidateData = $candidate->execute($id);

    $elements = array(
        'candidates' => $candidateData
    );

    echo $twig->render("detail.html.twig", $elements);
    die();
}

header('Location:index.php');