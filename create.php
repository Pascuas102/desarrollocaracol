<?php
/**
 * Created by PhpStorm.
 * User: JDP_1
 * Date: 15/08/2017
 * Time: 5:24 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ . '/vendor/autoload.php');

$loader = new \Twig_Loader_Filesystem(__DIR__ . "/templates");
$twig = new \Twig_Environment($loader, array(
    "cache" => __DIR__ . "/templates/compiled", "debug" => true, "autoescape" => true
));

if (isset($_POST['name'], $_POST['identification'], $_POST['age'], $_POST['salary'], $_POST['description'])) {
    $data = array('name' =>$_POST['name'],
        'identification' => $_POST['identification'],
        'age' => $_POST['age'],
        'salary' => $_POST['salary'],
        'description' => $_POST['description']
    );
    $newUser = new model\Application\createNewCandidateUseCase(
        new model\Infrastructure\MySQL\mySQLRepository(
            new model\Infrastructure\MySQL\mySQLConnection()
        )
    );

    if ($newUser->execute($data)) {
        echo $twig->render("register_concluded.html.twig");
        die();
    }
}

echo $twig->render("register.html.twig");