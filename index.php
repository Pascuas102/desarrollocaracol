<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__.'/vendor/autoload.php');

$loader = new \Twig_Loader_Filesystem(__DIR__."/templates");
$twig = new \Twig_Environment( $loader, array(
    "cache" => __DIR__."/templates/compiled", "debug" => true, "autoescape" => true
));

echo $twig->render("index.html.twig");