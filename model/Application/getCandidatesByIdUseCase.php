<?php

namespace model\Application;

use model\Infrastructure\MySQL\mySQLRepository;

class getCandidatesByIdUseCase
{
    private $repository;

    public function __construct(mySQLRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute($id)
    {
        return $this->repository->findById($id);
    }
}