<?php

namespace model\Application;


use model\Infrastructure\MySQL\mySQLRepository;

class getCandidateByDataBaseId
{
    private $repository;

    public function __construct(mySQLRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute($id)
    {
        return $this->repository->find($id);
    }
}