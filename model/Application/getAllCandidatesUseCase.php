<?php

namespace model\Application;
use model\Infrastructure\MySQL\mySQLRepository;
use \PDO;

class getAllCandidatesUseCase
{
    private $repository;

    public function __construct(mySQLRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute() {
        return $this->repository->all();
    }
}