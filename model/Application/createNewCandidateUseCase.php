<?php

namespace model\Application;

use model\Infrastructure\MySQL\mySQLRepository;

class createNewCandidateUseCase
{
    private $repository;

    public function __construct(mySQLRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(array $data) {
        return $this->repository->save($data);
    }
}