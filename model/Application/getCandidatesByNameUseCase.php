<?php

namespace model\Application;

use model\Infrastructure\MySQL\mySQLRepository;

class getCandidatesByNameUseCase
{
    private $repository;

    public function __construct(mySQLRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute($name)
    {
        return $this->repository->findByName($name);
    }
}