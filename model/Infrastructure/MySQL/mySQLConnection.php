<?php
/**
 * Created by PhpStorm.
 * User: JDP_1
 * Date: 15/08/2017
 * Time: 5:10 PM
 */

namespace model\Infrastructure\MySQL;
use \PDO;

class mySQLConnection
{
    protected $user = "root";
    protected $pass = "";
    protected $conection = 'mysql:host=localhost;dbname=caracolTest';
    protected $db;

    final public function connect() {
        $this->db = new PDO($this->conection,$this->user, $this->pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->db;
    }
}