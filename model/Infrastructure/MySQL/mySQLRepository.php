<?php

namespace model\Infrastructure\MySQL;

use \PDO;

class mySQLRepository
{
    private $repository;

    public function __construct(mySQLConnection $repository)
    {
        $this->repository = $repository->connect();
    }

    public function save(array $data)
    {
        $statement = $this->repository->prepare(
            "INSERT INTO candidate(fullname, identification, age, salary, description) 
             VALUES(:fullname, :identification, :age, :salary, :description)"
        );
        $statement->bindParam(":fullname", $data['name'], PDO::PARAM_STR);
        $statement->bindParam(":identification", $data['identification'], PDO::PARAM_STR);
        $statement->bindParam(":age", $data['age'], PDO::PARAM_INT);
        $statement->bindParam(":salary", $data['salary'], PDO::PARAM_INT);
        $statement->bindParam(":description", $data['description'], PDO::PARAM_STR);
        return $statement->execute();
    }

    public function find($id)
    {
        $statement = $this->repository->prepare("SELECT * FROM candidate WHERE id = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function all()
    {
        $statement = $this->repository->prepare("SELECT * FROM candidate");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findByName($name)
    {
        $statement = $this->repository->prepare('SELECT * FROM candidate WHERE fullname LIKE :fullname');
        $statement->bindParam(":fullname", $name, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById($id)
    {
        $statement = $this->repository->prepare("SELECT * FROM candidate WHERE identification = :identification");
        $statement->bindParam(":identification", $id, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}