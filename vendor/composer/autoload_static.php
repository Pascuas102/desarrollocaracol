<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcab2bdbc6fdc0f23bba91b312b8efc7c
{
    public static $prefixLengthsPsr4 = array (
        'm' => 
        array (
            'model\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'model\\' => 
        array (
            0 => __DIR__ . '/../..' . '/model',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcab2bdbc6fdc0f23bba91b312b8efc7c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcab2bdbc6fdc0f23bba91b312b8efc7c::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitcab2bdbc6fdc0f23bba91b312b8efc7c::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
